from textx import metamodel_from_str, get_children_of_type

domain = """
Domain: name=ID
'(:types' types*=Type')'
'(:predicates' predicates*=DefinedPredicate ')'
'(:actions' actions*=Action ')'
'(:constraints' constraints*=Constraint ')';
Type: '(:def-type' name=ID type*=ID fields*=Parameter[','] ')';
Parameter: '?'var=ID '-' ID | '?'var=ID '-' PrimitiveType ',' '?'var=ID '-' "Unit";
PrimitiveType: "INT" | "BOOL" | "Unit";
Unit: "mg" | "g" | "L" | "mL" | "M";
DefinedPredicate: '(' name=ID parameters*=Parameter ')';
Action: '(' name=ID
    '(:parameters' parameters*=Parameter[','] ')'
    '(:precondition' predicates*=Predicate ')'
    '(:effect' predicates*=Predicate ')'
')';
Predicate: '(' logic*=Logic ')';
PParameter: '?'var=ID;
Logic: AND | NOT | OR | name=ID parameters*=PParameter;
AND: 'AND' predicates*=Predicate;
NOT: 'NOT' Predicate;
OR: 'OR' predicates*=Predicate;
Constraint: '(' name=ID
                '(:parameters' parameters*=Parameter[','] ')'
                '(' logic*=AdditionalLogic ')'')';
AdditionalLogic: AND | NOT | OR | FORALL | ATLEASTONE | ATMOSTONCE;
FORALL: 'FORALL' parameters*=PParameter '(' AdditionalLogic ')';
ATMOSTONCE: 'ATMOSTONCE' Predicate;
ATLEASTONE: 'ATLEASTONCE' Predicate;
"""

problem = """
Problem: name=ID
    '(:domain' name=ID ')'
    '(:objects' objects*=Object[',']')'
    '(:init' State ')'
    '(:goal' State ')';
Object: name*=ID '-' InstantiateType;
InstantiateType: ID | '(' name=ID vals*=Primitive ')';
Primitive: INT | Unit;
State: Predicate;
"""

grammar = f"""
ChemistryPDDL: '(make-planner'
                '(:domain' Domain')'
                '(:problem' Problem '))';
{domain}
{problem}
"""

mm = metamodel_from_str(grammar)

