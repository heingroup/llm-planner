from unified_planning.io import PDDLReader, PDDLWriter
from unified_planning import engines
from unified_planning.shortcuts import *

UR_DOMAIN_FILEPATH = "ur.pddl"
UR_PROBLEM_FILEPATH = "ur-problem.pddl"

def pddl_to_unified_planner(domain_pddl: str, problem_pddl: str):

    reader = PDDLReader()
    return reader.parse_problem(domain_pddl, problem_pddl)

def domain_pddl_to_python(domain_pddl: str):
    return PDDLReader().parse_problem(domain_pddl, None)

def try_to_solve(problem):
    with OneshotPlanner(name='pyperplan') as planner:
        result = planner.solve(problem)
        if result.status == engines.PlanGenerationResultStatus.SOLVED_SATISFICING:
            print("Pyperplan returned: %s" % result.plan)
        else:
            print("No plan found.")

print(try_to_solve(pddl_to_unified_planner(UR_DOMAIN_FILEPATH, UR_PROBLEM_FILEPATH)))
