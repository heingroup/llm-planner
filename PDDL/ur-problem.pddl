(define

(problem solubility)

(:domain ur)

(:objects
l1 l2 l3 l4 l5 l6 - location
analyte - solid
solvent - liquid
v1 v2 v3 v4 v5 v6 - vial
)

(:init
    (inuse v1)
    (at v1 l1)
    (= (to-add analyte v1) 50)
)

(:goal (and (analyzedhplc v1) (at v1 l2) (= (added v1 analyte) 50)))
)
