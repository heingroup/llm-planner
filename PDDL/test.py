from unified_planning.io import PDDLReader, PDDLWriter

test_domain = """
(define (domain fn-counters)
    (:types
        counter
    )

    (:functions
        (value ?c - counter);; - int  ;; The value shown in counter ?c
        (max_int);; -  int ;; The maximum integer we consider - a static value
    )

    ;; Increment the value in the given counter by one
    (:action increment
        :parameters (?c - counter)
        :precondition (and (<= (+ (value ?c) 1) (max_int)))
        :effect (and (increase (value ?c) 1))
    )

    ;; Decrement the value in the given counter by one
    (:action decrement
        :parameters (?c - counter)
        :precondition (and (>= (value ?c) 1))
        :effect (and (decrease (value ?c) 1))
    )
)
"""

test_problem = """
(define (problem instance_4)
  (:domain fn-counters)
  (:objects
    c0 c1 c2 c3 - counter
  )

  (:init
    (= (max_int) 10)
    (= (value c0) 0)
    (= (value c1) 0)
    (= (value c2) 0)
    (= (value c3) 0)
  )

  (:goal
    (and
      (<= (+ (value c0) 1) (value c1))
      (<= (+ (value c1) 1) (value c2))
      (<= (+ (value c2) 1) (value c3))
    )
  )

)
"""

def pddl_to_unified_planning():
    reader = PDDLReader()
    pddl_problem = reader.parse_problem(test_domain, test_problem)
