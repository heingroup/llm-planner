from unified_planning.io import PDDLReader, PDDLWriter
from unified_planning import engines
from unified_planning.shortcuts import *

UR_DOMAIN_FILEPATH = "ur.pddl"
UR_PROBLEM_FILEPATH = "ur-problem.pddl"

def pddl_to_unified_planner_problem(domain_pddl: str, problem_pddl: str):

    reader = PDDLReader()
    return reader.parse_problem(domain_pddl, problem_pddl)

def domain_pddl_to_python(domain_pddl: str):
    return PDDLReader().parse_problem(domain_pddl, None)

def initialize_fluents(problem):
    numeric_fluents = [f.type for f in problem.fluents]
    return [f.type for f in problem.fluents]

def numeric_solve(problem):
    with OneshotPlanner(name='enhsp') as planner:
        result = planner.solve(problem)
        plan = result.plan
        if plan is not None:
            print("%s returned:" % planner.name)
            print(plan)
        else:
            print("No plan found.")

print(initialize_fluents(pddl_to_unified_planner_problem(UR_DOMAIN_FILEPATH, UR_PROBLEM_FILEPATH)))
print(numeric_solve(pddl_to_unified_planner_problem(UR_DOMAIN_FILEPATH, UR_PROBLEM_FILEPATH)))
