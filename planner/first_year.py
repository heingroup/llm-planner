'''
PDDL for first-year chemistry experiments:
'''

domain = '''
(define (domain first-year)
    (:requirements :strips :typing)
    (:types glassware chemical technique - object
            beaker erlynmyler roundbottom buret - glassware
            solid liquid - chemical)
    (:functions
        (amount-remaining ?c - chemical))
    (:predicates
        (clean ?g - glassware)
        (in-use ?g - glassware))
    (:action DOSE-SOLID
        :parameters (?s - solid ?g - glassware ?n - number)
        :precondition (and (clean ?g) (not (in-use ?g)) (> (amount-remaining ?s) n))
        :effect (in-use ?g) )
    (:action DOSE-LIQUID
        :parameters (?l - liquid ?g - glassware ?n - number)
        :precondition (and (clean ?g) (not (in-use ?g)) (> (amount-remaining ?l) ?n))
        :effect (in-use ?g))
    (:action WASH-GLASSWARE
        :parameters (?g - glassware)
        :precondition (and (not (in-use ?g)) (not (clean ?g)))
        :effect))
    '''

problem = '''
    (define
    (problem ...)
    (:domain first-year)
    (:objects ...)
    (:init ...)
    (:goal ...))
    '''
