(define (domain ur)

(:requirements :strips :typing :numeric-fluents :constraints)

(:types glassware chemical location int - objects
                solution liquid solid - chemical
                mL L - liquid
                M - solution
                mg g - solid
                vial - glassware)

(:predicates
            (inuse ?g - glassware)
            (clean ?g - glassware)
            (at ?g - glassware ?l - location)
            (shaken ?g - glassware)
            (analyzedhplc ?g - glassware)
            (unknown-add ?c - chemical ?g - glassware)
            (capped ?g - glassware)
            (avalible ?c - chemical)
            (contains-chemicals ?g - glassware)
            (readyforanalysis ?g - glassware)
)


(:functions
            (added ?g - glassware ?c - chemical) ;; checks if the provided chemical is added in that glassware
            (max-volume ?g - glassware) ;; the maximum volume for a specific type of glassware
            (to-add ?c - chemical ?g - glassware) ;; to specify amount of solution, liquid or solid to be added to a specific glassware
            (given ?c -chemical) ;; to specify any chemical that is already present
            (duplicates) ;; if duplicates are to be run
)
        (:action setreadytoanalyse
                 :parameters (?v - vial ?l - location)
                 :precondition (and (at ?v ?l) (forall (?c - chemical) (= (to-add ?c ?v) 0)))
                 :effect (readyforanalysis ?v))

        (:action dosesolid
                :parameters (?s - solid ?v - vial ?l - location)
                :precondition (AND (at ?v ?l) (NOT (capped ?v)) (> (given ?s) 0) (> (to-add ?s ?v) 0))
                :effect (AND (assign (added ?v ?s) (to-add ?s ?v)) (assign (to-add ?s ?v) 0)))

        (:action doseliquid
                :parameters (?l - liquid ?v - vial ?loc - location)
                :precondition (AND (at ?v ?loc) (NOT (capped ?v)) (> (given ?l) 0) (> (to-add ?l ?v) 0))
                :effect (AND (assign (added ?v ?l) (to-add ?l ?v)) (assign (to-add ?l ?v) 0)))

        (:action movevial
                :parameters (?v - vial ?l1 - location ?l2 - location)
                :precondition (at ?v ?l1)
                :effect (AND (at ?v ?l2) (NOT (at ?v ?l1))))

        (:action shake
                :parameters (?v - vial ?l - location)
                :precondition (at ?v ?l)
                :effect (shaken ?v))

        (:action dilute
                :parameters (?v - vial ?given - chemical ?desired - chemical ?l - location)
                :precondition (AND (NOT (inuse ?v)) (at ?v ?l))
                :effect (assign (added ?v ?desired) 10))

        (:action runhplc
                :parameters (?v - vial ?l - location)
                :precondition (AND (at ?v ?l) (shaken ?v) (readyforanalysis ?v))
                :effect (analyzedhplc ?v))

        (:action cap
                :parameters (?g - glassware ?l - location)
                :precondition (AND (at ?g ?l) (NOT (capped ?g)))
                :effect (capped ?g))

        (:action uncap
                :parameters (?g - glassware ?l - location)
                :precondition (AND (at ?g ?l) (capped ?g))
                :effect (NOT (capped ?g)))


)
