(define

(problem solubility)

(:domain ur)

(:objects
l1 l2 l3 l4 l5 l6 - location
analyte - solid
solvent - liquid
v1 v2 v3 v4 v5 - vial
)

(:init
    (inuse v1)
    (at v1 l1)
    (at v2 l2)
    (at v3 l3)
    (at v4 l4)
    (at v5 l5)
    (= (added v1 analyte) 0)
    (= (added v2 analyte) 0)
    (= (added v3 analyte) 0)
    (= (added v4 analyte) 0)
    (= (added v5 analyte) 0)
    (= (added v1 solvent) 0)
    (= (added v2 solvent) 0)
    (= (added v3 solvent) 0)
    (= (added v4 solvent) 0)
    (= (added v5 solvent) 0)
    (= (max-volume v1) 100)
    (= (max-volume v2) 100)
    (= (max-volume v3) 100)
    (= (max-volume v4) 100)
    (= (max-volume v5) 100)
    (= (to-add analyte v1) 50)
    (= (to-add analyte v2) 0)
    (= (to-add analyte v3) 0)
    (= (to-add analyte v4) 0)
    (= (to-add analyte v5) 0)
    (= (to-add solvent v1) 40)
    (= (to-add solvent v2) 0)
    (= (to-add solvent v3) 0)
    (= (to-add solvent v4) 0)
    (= (to-add solvent v5) 0)
    (= (given analyte) 200)
    (= (given solvent) 500)
    (= (duplicates) 0)
)

(:goal (and (at v1 l2) (= (added v1 solvent) 40) (= (added v1 analyte) 50) (analyzedhplc v1) ))
)
