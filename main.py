import ollama
import PDDL.planner as planner

PERSONAL_IP = "127.0.0.1:11434"
HEIN_IP = "137.82.65.246"
# TODO: try json schema tomorw: https://json-schema.org/
# see full grammar here: https://github.com/antlr/grammars-v4/blob/master/pddl/Pddl.g4

# also need reminding the LLM about the original problem
# or turn to other types of NLP techniques

def extract_args(arg_to_extract:str, pddl: str):
    "TODO"

def get_tags(simple_english_step: str):
    "TODO"

def prose_to_synthethic_chemistry_procedure(domain_pddl: str, problem_pddl: str, model:str, design: str, file_name:str):
    actions = [action.name for action in planner.domain_pddl_to_python(domain_pddl).actions]

    def send_llm_message(message:str):
       return ollama_client.chat(model=model, messages=[
           {
        'role': 'user',
        'content': message
           }])['message']['content']

    ollama_client = ollama.Client(host=HEIN_IP)

    print("I will create a step-by-step plan of this design: \n"
          + design
          + "\n and then try to express the plan in the notation of synthethic chemistry procedures.")

    satisfied = False
    mentioned_numbers = [int(i) for i in design.split() if i.isdigit()] # extract numbers mentioned in the prompt
    print(mentioned_numbers) # TODO

    step_by_step_plan = ""
    while not satisfied:
        step_by_step_plan = send_llm_message(f"""
            Please create a step-by-step plan in third-person past-passive tense for this chemistry prompt {design}.
            Don't format the text.
            Do NOT USE ANY NUMBERS THAT HAVE NOT BEEN PROVIDED TO YOU, IF THEY ARE UNKNOWN, THEN LIST THE AMOUNTS AS UNKNOWN.
            Each action must have their own step.

            Here is an example:
            1. Potassium carbonate (0.63 g, 4.56 mmol) and thiophenol (0.19 g, 1.69 mmol) were added to the 2-nitrobenzene sulfonamide 50 (0.50 g, 1.302 mmol) in N, N-dimethylformamide (33 mL) at room temperature and the mixture was stirred for 16 h.
            2. Deionised water (50 mL) was added and the aqueous phase was extracted with ethyl acetate (5 × 50 mL).
        """)

        simple_english_plan_valid = False
        while not simple_english_plan_valid:
            numbers_in_generated_plan = [int(i) for i in step_by_step_plan.split() if i.isdigit()]

            valid = input(step_by_step_plan + "Does this plan make sense?")
            if True:
                "TODO"
            elif ("n" in str(valid).lower()):
                feedback = input("What should be changed?")
                step_by_step_plan = send_llm_message(f"Please change {step_by_step_plan}, so that {feedback}.")

            elif ("y" in str(valid).lower()):
                simple_english_plan_valid = True

        split_steps = step_by_step_plan.splitlines()
        tokenized_steps = [get_tags(step) for step in split_steps]

        pddl_actions_plan_valid = False
        while not pddl_actions_plan_valid:
            valid = input(send_llm_message(f"""Based on these robot actions: {actions}, for each step in {step_by_step_plan}, map that step to a given action.
            YOU CAN ONLY USE PROVIDED ACTIONS. You may use more than one action if required.""") + " Do these steps make sense?")
            if ("n" in str(valid).lower()):
                "TODO"
            if ("y" in str(valid).lower()):
                pddl_actions_plan_valid = True


        synthethic_plan_file = open(file_name, "a+")
        synthethic_plan_file.write(step_by_step_plan)
        synthethic_plan_file.close()

        satisfied = True

if __name__ == "__main__":
    standard_curve_design = """Prepare two 20 mL solutions of HCl of concentrations of 0.25 M and 0.50 M by diluting a stock solution of HCl (1 M).
    After the 2 solutions are made, shake each solution, then run each solution through HPLC."""

    variation_language = """10 mL of HCl (2 M) was added to 5 g NaCl, 5 mL of 1 M NaOH, and 7 mL water at room temperature, stirred for 10 h. After, another 10 mL of 2 M HCl was added, and HPLC was run."""
    variation_language_1 = """10 mL of 2 M HCl was added to 5 g NaCl, 5 mL of 1 M NaOH, and 7 mL water at room temperature, stirred for 10 h. After, another 10 mL of 2 M HCl was added, and HPLC was run."""

    prose_to_synthethic_chemistry_procedure(domain_pddl=planner.UR_DOMAIN_FILEPATH,
                                            problem_pddl=planner.UR_PROBLEM_FILEPATH,
                                            model="llama3",
                                            design=standard_curve_design,
                                            file_name="multi_step_problem_pddl.txt")
