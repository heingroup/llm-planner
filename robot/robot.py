"""
The main class for robot.
"""
from unified_planning.io import PDDLReader
from unified_planning.shortcuts import *

from action import Action

UR_DOMAIN_FILEPATH = "../pddl/ur.pddl"
UR_PROBLEM_FILEPATH = "../pddl/ur-problem.pddl"

class RobotActionGenerator:
    def __init__(self, solver_type: str="enhsp") -> None:
        self.initial = [] # inital state of the robot
        self.actions_and_state = None # sequence of actions with associated state
        self.goal = [] # predicates describing the goal
        self.actions: list[Action] = [] # actions that the robot can take
        self.planner = OneshotPlanner(name=solver_type)
        self.reader = PDDLReader()

    def pddl_to_unified_planner_problem(self, domain_pddl: str, problem_pddl: str):
            return self.reader.parse_problem(domain_pddl, problem_pddl)

    def domain_pddl_to_python(self, domain_pddl: str):
            return self.reader.parse_problem(domain_pddl, None)

    def initialize_fluents(self, domain_pddl: str, initial_objects):
            all_fluents = self.domain_pddl_to_python(domain_pddl).fluents
            numeric_fluents = [f for f in all_fluents if f.type.is_real_type()]
            "TODO"
            print(initial_objects)
            return numeric_fluents

    def set_up_goal_state():
        "TODO"

    def numeric_solve(self, problem):
            with OneshotPlanner(name='enhsp') as planner:
                result = self.planner.solve(problem)
                plan = result.plan
            if plan is not None:
                print("%s returned:" % planner.name)
                return plan
            else:
                print("No plan found.")

    def solver_actions_to_python():
        "TODO"

robot = RobotActionGenerator()
robot.numeric_solve(robot.pddl_to_unified_planner_problem(UR_DOMAIN_FILEPATH, UR_PROBLEM_FILEPATH))
